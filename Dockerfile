# start by pulling the python image
FROM registry.access.redhat.com/ubi8/python-36:1-208.1683014496

# copy the requirements file into the image
COPY ./requirements.txt /app/requirements.txt

# copy the openapi spec file into the image
COPY ./openapi-spec.json /app/openapi-spec.json

# switch working directory
WORKDIR /app


# update pip
RUN pip install --upgrade pip

# install the dependencies and packages in the requirements file
RUN pip install -r requirements.txt

# copy every content from the local file to the image
COPY . /app

# configure the container to run in an executed manner
ENTRYPOINT [ "python" ]

CMD ["application.py" ]