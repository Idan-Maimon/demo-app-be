import os
import json
import unittest
from unittest.mock import patch, MagicMock
from application import app, Drink

class FlaskUnitTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        app.config['TESTING'] = True
        cls.client = app.test_client()

    def test_index(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'This is my bar menue, please press here', response.data)

    @patch('builtins.open', new_callable=unittest.mock.mock_open, read_data=json.dumps({"info": "test schema"}))
    def test_get_openapi_schema(self, mock_open):
        response = self.client.get('/api/openapi.json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')
        data = json.loads(response.data)
        self.assertEqual(data, {"info": "test schema"})

    @patch('application.Drink.query')
    def test_get_drinks(self, mock_query):
        # Mock the Drink model's query to return a mock drink
        mock_drink = Drink(name='TestDrink', description='TestDescription')
        mock_query.all.return_value = [mock_drink]

        response = self.client.get('/api/drinks')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.data)
        self.assertIsInstance(data['drinks'], list)
        self.assertEqual(len(data['drinks']), 1)
        self.assertEqual(data['drinks'][0]['name'], 'TestDrink')

    @patch('application.Drink.query')
    @patch('application.db.session')
    def test_add_drink(self, mock_db_session, mock_query):
        # Mock the add and commit methods
        mock_db_session.add.return_value = None
        mock_db_session.commit.return_value = None

        response = self.client.post('/api/drinks', json={
            'name': 'Coke',
            'description': 'Refreshing soda'
        })
        
        # Assert that the response status code is 201 (Created)
        self.assertEqual(response.status_code, 201)
        
        # Check that the response contains the ID of the created drink
        data = json.loads(response.data)
        self.assertIn('id', data)

    def test_add_drink_missing_fields(self):
        response = self.client.post('/api/drinks', json={
            'name': 'Pepsi'
        })
        self.assertEqual(response.status_code, 400)
        data = json.loads(response.data)
        self.assertIn('error', data)

    @patch('application.Drink.query')
    def test_delete_drink(self, mock_query):
        # Mock the query to return None, simulating a drink that does not exist
        mock_query.get.return_value = None
        
        drink_id = 999  # ID of the drink that doesn't exist
        response = self.client.delete(f'/api/drinks/{drink_id}')
        
        # Assert the response status code is 404
        self.assertEqual(response.status_code, 404)
        
        # Load the response data and assert it contains the error message
        data = json.loads(response.data)
        self.assertIn('error', data)
        self.assertEqual(data['error'], "Drink Not Found")

if __name__ == '__main__':
    unittest.main()