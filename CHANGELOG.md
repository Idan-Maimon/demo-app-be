# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [v2.9.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.9.0) - 2025-02-19

- [`28e5480`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/28e5480fda8623094ec1c312d9e3770b559ad63c) feat: test2
- [`532073f`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/532073f4bc5923c36ad5a98b616b702e849cec5b) feat: test1

## [v2.9.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.9.0) - 2025-02-19

- [`28e5480`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/28e5480fda8623094ec1c312d9e3770b559ad63c) feat: test2
- [`532073f`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/532073f4bc5923c36ad5a98b616b702e849cec5b) feat: test1

## [v2.8.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.8.0) - 2025-02-19

- [`423b563`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/423b563ca21bd02174d76798723ed9b2b0ee49e1) test-branch
- [`8bf23e3`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/8bf23e37feb9836908ba33eb1356caa3f8f73f5c) test-branch
- [`95b8c86`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/95b8c868783e7f416c9c27b840cdb54eec760a47) feat: branch-test1

## [v2.7.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.7.0) - 2025-02-19

- [`95b8c86`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/95b8c868783e7f416c9c27b840cdb54eec760a47) feat: branch-test1

## [v2.6.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.6.0) - 2025-02-17

- [`107f5fc`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/107f5fcbc502aa1d021d00a55e61c177aab77c74) feat: test release5
- [`2bfe4b0`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/2bfe4b0680581da876b8d6642652a7edd332c843) feat: test release5
- [`5162597`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/51625979e5f5bb48d57f7bd92d99763a7b753283) Merge commit '67b00ad849b8518ddcfd7d6d0fd524be4f672adc' into release
- [`21764c5`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/21764c561c3de168e5a24bbf3aee832ec18be182) feat: test release4

## [v2.4.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.4.0) - 2025-02-17

- [`a25934a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/a25934a7bc0c1f4fc77fc5b887e264d9de9c0047) feat: testing uplift6

## [v2.3.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.3.0) - 2025-02-17

- [`4bd37f0`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/4bd37f083eb10cae24252e753598924ab4226e7a) feat: testing uplift5
- [`d01f727`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d01f7273bde2eeb6128886c9b69c502cbedbb049) feat: testing uplift4

## [v2.2.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.2.0) - 2025-02-17

- [`bbada86`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/bbada86a4c045652038683981f8a0de2d465c7c5) feat: testing uplift3
- [`ff5b3b2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/ff5b3b266a58162a64fe345b0fe452075736f330) feat: testing uplift2

## [v2.2.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.2.0) - 2025-02-17

- [`bbada86`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/bbada86a4c045652038683981f8a0de2d465c7c5) feat: testing uplift3
- [`ff5b3b2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/ff5b3b266a58162a64fe345b0fe452075736f330) feat: testing uplift2

## [v2.1.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.1.0) - 2025-02-17

- [`0b59b2a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/0b59b2a75c48b0bea66141c945ebccda60f31191) feat: testing uplift
- [`d72a8ad`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d72a8add17ce232e55dcc1f52cf7e92d8245424b) feat: uplift

## [v2.0.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.0.0) - 2025-02-17

- [`61ebc0a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/61ebc0a95497b51d2d18ef70d0065cb1aac7a2db) feat: edit ci
- [`de3d0f2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/de3d0f290e98bd0328f1b8ace054142be4ef09a7) feat: edit ci
- [`be9f26c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/be9f26cd5505d052e6d93aa3663c84622ab29546) feat: edit ci
- [`5a00d33`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5a00d33f75d909379a61f0c51cdf222477b9f9c3) feat: edit ci
- [`e03a020`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e03a020201b3c7d6ce824e1d271c2e6817c6e545) feat: edit ci
- [`822fac7`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/822fac77f6da4cb114b24abd38f3b934921d45f5) feat: edit ci
- [`eb775a9`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/eb775a9507f885ad51f7b39a602bb03f62839f14) feat: edit ci
- [`dde36fd`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/dde36fd2790f6fa4657e6e9d9e5a5f17d04e9f41) feat: edit ci
- [`bd6a535`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/bd6a53528c8fb558e10a486a58d0995daa4585b7) feat: edit ci
- [`48d5789`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/48d5789c98ae3294ed1bbe0c7bedc90809743fd9) feat: edit ci
- [`3e98a53`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/3e98a539f26e8a2aecf3dafcd7ab449ddfb464a6) feat: edit ci
- [`2956c7e`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/2956c7e9ef49c9d6ce796506d338d51fef00df02) feat: edit ci
- [`bd0825b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/bd0825b2bd438c8db0bd0f1ff6aee2adc9c69882) feat: edit ci
- [`71b2614`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/71b26141713ca5cb9e652fc0f49dd29671bca218) feat: edit ci
- [`fe0d037`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/fe0d037997ecf5ed9f7ef7857a23398eb10071bd) feat: editing ci
- [`7b55b81`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/7b55b81ffd690df936c23d10a10419fcee77e6b7) feat: editing ci
- [`61674b7`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/61674b7440c56ab7f465dbea3bad2033cb517415) feat: editing ci
- [`07dfc30`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/07dfc30ee82175a2c72fc11f1aa2dda63f9e8a5b) feat: editing ci
- [`269c990`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/269c9903283cb9cc498b8ba9a06549d3617dea63) feat: editing ci
- [`65f150b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/65f150b76e63621781d05fc3c2e25d0b221f557b) fix: fixing gitlab ci
- [`627e010`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/627e010fb78ed55a92faa9aa27750b9b65a0d6cb) fix: fixing gitlab ci
- [`623bb76`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/623bb76278fd240c619cda3e86b618b7ac62ffc4) fix: fixing gitlab ci
- [`c26536a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/c26536a5f2bb20efaadd316752243200398da09e) fix: fixing gitlab ci
- [`45df884`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/45df8842352d7c4fe878932616426b363360fe8a) fix: fixing gitlab ci
- [`d7048d4`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d7048d482f88a87608de19d8fa7f901257c981e6) fix: fixing gitlab ci
- [`734e2b8`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/734e2b88531401bcf12d41344d4655bb5af4df67) fix: fixing gitlab ci
- [`e1e6ee4`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e1e6ee443a38089b110e61601832e293b828da2c) fix: fixing gitlab ci
- [`e46341b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e46341b7d7835b2f1e325f14dac587b42451f7df) fix: fixing gitlab ci
- [`43fdb91`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/43fdb9145b73f6c7db00ad80b63c719604df5ad8) fix: fixing IMAGE_TAG variable
- [`4c84f06`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/4c84f068982f1a94824f760a63c670ae3c782673) fix: fixing IMAGE_TAG variable
- [`d0b5b90`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d0b5b9050037d4ae9251cb86cb34a36262b376e8) fix: fixing IMAGE_TAG variable
- [`dbcd3e2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/dbcd3e276c12100ce62878146e9b8bec224b33bc) fix: fixing IMAGE_TAG variable
- [`2099811`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/20998117065a0ddbd91cd09f4024834ebc97f7eb) fix: fixing IMAGE_TAG variable
- [`34e429a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/34e429a37a92370f10ac60e690371050a3385e14) fix: fixing IMAGE_TAG variable
- [`836b17f`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/836b17ffc6eb566dd8bec4794274b44c0c5adab1) fix: fixing IMAGE_TAG variable
- [`93b8e90`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/93b8e902065841e134e2c80c65b16b43894be9bc) Update .gitlab-ci.yml file
- [`7a624df`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/7a624dfa6693099470c12a5f0d78671f2a1c486d) fix: fixing IMAGE_TAG variable
- [`18261e6`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/18261e63a47a58d4a338ccb7b654cc6406b73a1a) fix: fixing IMAGE_TAG variable
- [`8b823a0`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/8b823a04b84afa5583a9a28a4deafb6ff03ba3e9) fix: fixing IMAGE_TAG variable
- [`9f6f976`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/9f6f976aae1a42d21a61e000e840439bf2094c90) fix: fixing IMAGE_TAG variable
- [`c671540`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/c67154041a3c7dab121ef0b9ddfbce40883f0fd9) fix: fixing IMAGE_TAG variable
- [`1d03b75`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/1d03b754bf0e2f14e7164d88c740cea26c12e9ab) feat: Adding release feature
  BREAKING CHANGE: new feature that changes how CI works
- [`b4f14b3`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b4f14b312cf35409339aeff2baedf300203422bc) feat\!: this is a new test17
- [`0d3b867`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/0d3b8671e1d41b32ec60c66a1e71553fc518fe87) feat{==!==}: this is a new test16

## [v2.0.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v2.0.0) - 2025-02-17

- [`61ebc0a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/61ebc0a95497b51d2d18ef70d0065cb1aac7a2db) feat: edit ci
- [`de3d0f2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/de3d0f290e98bd0328f1b8ace054142be4ef09a7) feat: edit ci
- [`be9f26c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/be9f26cd5505d052e6d93aa3663c84622ab29546) feat: edit ci
- [`5a00d33`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5a00d33f75d909379a61f0c51cdf222477b9f9c3) feat: edit ci
- [`e03a020`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e03a020201b3c7d6ce824e1d271c2e6817c6e545) feat: edit ci
- [`822fac7`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/822fac77f6da4cb114b24abd38f3b934921d45f5) feat: edit ci
- [`eb775a9`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/eb775a9507f885ad51f7b39a602bb03f62839f14) feat: edit ci
- [`dde36fd`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/dde36fd2790f6fa4657e6e9d9e5a5f17d04e9f41) feat: edit ci
- [`bd6a535`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/bd6a53528c8fb558e10a486a58d0995daa4585b7) feat: edit ci
- [`48d5789`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/48d5789c98ae3294ed1bbe0c7bedc90809743fd9) feat: edit ci
- [`3e98a53`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/3e98a539f26e8a2aecf3dafcd7ab449ddfb464a6) feat: edit ci
- [`2956c7e`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/2956c7e9ef49c9d6ce796506d338d51fef00df02) feat: edit ci
- [`bd0825b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/bd0825b2bd438c8db0bd0f1ff6aee2adc9c69882) feat: edit ci
- [`71b2614`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/71b26141713ca5cb9e652fc0f49dd29671bca218) feat: edit ci
- [`fe0d037`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/fe0d037997ecf5ed9f7ef7857a23398eb10071bd) feat: editing ci
- [`7b55b81`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/7b55b81ffd690df936c23d10a10419fcee77e6b7) feat: editing ci
- [`61674b7`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/61674b7440c56ab7f465dbea3bad2033cb517415) feat: editing ci
- [`07dfc30`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/07dfc30ee82175a2c72fc11f1aa2dda63f9e8a5b) feat: editing ci
- [`269c990`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/269c9903283cb9cc498b8ba9a06549d3617dea63) feat: editing ci
- [`65f150b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/65f150b76e63621781d05fc3c2e25d0b221f557b) fix: fixing gitlab ci
- [`627e010`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/627e010fb78ed55a92faa9aa27750b9b65a0d6cb) fix: fixing gitlab ci
- [`623bb76`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/623bb76278fd240c619cda3e86b618b7ac62ffc4) fix: fixing gitlab ci
- [`c26536a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/c26536a5f2bb20efaadd316752243200398da09e) fix: fixing gitlab ci
- [`45df884`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/45df8842352d7c4fe878932616426b363360fe8a) fix: fixing gitlab ci
- [`d7048d4`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d7048d482f88a87608de19d8fa7f901257c981e6) fix: fixing gitlab ci
- [`734e2b8`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/734e2b88531401bcf12d41344d4655bb5af4df67) fix: fixing gitlab ci
- [`e1e6ee4`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e1e6ee443a38089b110e61601832e293b828da2c) fix: fixing gitlab ci
- [`e46341b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e46341b7d7835b2f1e325f14dac587b42451f7df) fix: fixing gitlab ci
- [`43fdb91`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/43fdb9145b73f6c7db00ad80b63c719604df5ad8) fix: fixing IMAGE_TAG variable
- [`4c84f06`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/4c84f068982f1a94824f760a63c670ae3c782673) fix: fixing IMAGE_TAG variable
- [`d0b5b90`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d0b5b9050037d4ae9251cb86cb34a36262b376e8) fix: fixing IMAGE_TAG variable
- [`dbcd3e2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/dbcd3e276c12100ce62878146e9b8bec224b33bc) fix: fixing IMAGE_TAG variable
- [`2099811`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/20998117065a0ddbd91cd09f4024834ebc97f7eb) fix: fixing IMAGE_TAG variable
- [`34e429a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/34e429a37a92370f10ac60e690371050a3385e14) fix: fixing IMAGE_TAG variable
- [`836b17f`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/836b17ffc6eb566dd8bec4794274b44c0c5adab1) fix: fixing IMAGE_TAG variable
- [`93b8e90`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/93b8e902065841e134e2c80c65b16b43894be9bc) Update .gitlab-ci.yml file
- [`7a624df`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/7a624dfa6693099470c12a5f0d78671f2a1c486d) fix: fixing IMAGE_TAG variable
- [`18261e6`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/18261e63a47a58d4a338ccb7b654cc6406b73a1a) fix: fixing IMAGE_TAG variable
- [`8b823a0`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/8b823a04b84afa5583a9a28a4deafb6ff03ba3e9) fix: fixing IMAGE_TAG variable
- [`9f6f976`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/9f6f976aae1a42d21a61e000e840439bf2094c90) fix: fixing IMAGE_TAG variable
- [`c671540`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/c67154041a3c7dab121ef0b9ddfbce40883f0fd9) fix: fixing IMAGE_TAG variable
- [`1d03b75`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/1d03b754bf0e2f14e7164d88c740cea26c12e9ab) feat: Adding release feature
  BREAKING CHANGE: new feature that changes how CI works
- [`b4f14b3`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b4f14b312cf35409339aeff2baedf300203422bc) feat\!: this is a new test17
- [`0d3b867`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/0d3b8671e1d41b32ec60c66a1e71553fc518fe87) feat{==!==}: this is a new test16

## [v1.2.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v1.2.0) - 2025-02-12

- [`59b272e`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/59b272e9a19bd76917f5782dc7feaf93fce4d6d8) feat: this is a new test15
- [`e2b1dcc`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e2b1dcc69ca8ae4de634bad1451348ab539af6e9) fix: this is a new test14
- [`5b1615b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5b1615b00515f6398462dab9aa5bf74fa2d9b757) feat{==!==}: this is a new test13
- [`b8e9e4b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b8e9e4b80c9f9823fe4fab17a6da779b7f0ffbda) feat: this is a new test12

## [v1.2.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v1.2.0) - 2025-02-12

- [`59b272e`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/59b272e9a19bd76917f5782dc7feaf93fce4d6d8) feat: this is a new test15
- [`e2b1dcc`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e2b1dcc69ca8ae4de634bad1451348ab539af6e9) fix: this is a new test14
- [`5b1615b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5b1615b00515f6398462dab9aa5bf74fa2d9b757) feat{==!==}: this is a new test13
- [`b8e9e4b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b8e9e4b80c9f9823fe4fab17a6da779b7f0ffbda) feat: this is a new test12

## [v1.2.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v1.2.0) - 2025-02-12

- [`59b272e`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/59b272e9a19bd76917f5782dc7feaf93fce4d6d8) feat: this is a new test15
- [`e2b1dcc`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e2b1dcc69ca8ae4de634bad1451348ab539af6e9) fix: this is a new test14
- [`5b1615b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5b1615b00515f6398462dab9aa5bf74fa2d9b757) feat{==!==}: this is a new test13
- [`b8e9e4b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b8e9e4b80c9f9823fe4fab17a6da779b7f0ffbda) feat: this is a new test12

## [v1.1.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v1.1.0) - 2025-02-12

- [`1172bea`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/1172bea2a90f3808c38e8d14d069dc32c6f83b47) feat: this is a new test11

## [v1.0.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v1.0.0) - 2025-02-12

- [`efeba38`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/efeba38b3c8af189f00bbad455ef0f347153d465) ci(uplift): uplifted for version v0.6.0
- [`ae27b85`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/ae27b857f7999bf1729a3f095d651a99fc89faa8) ci(uplift): uplifted for version v1.0.0
- [`da11058`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/da11058799f09f4c1e628c0d7d726ba52902a307) feat: this is a new test10
  BREAKING CHANGE: SOME changes
- [`cbb0926`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/cbb0926dac1a1a4db4e10b0829a43b5a27b853df) BREAKING CHANGE: this is a new test9
  hello 1
  HELLO 2
  Hello 3
- [`33e1690`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/33e1690dc2f24c11f70eb4026d3508831a5088c4) ci(uplift): uplifted for version v0.7.0
- [`00a5ed2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/00a5ed27cf417f6f94715a71196e3d51ec29ed69) feat: this is a new test8
- [`e81edcd`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e81edcd9215cdae93b3e70404968150055414bab) ci(uplift): uplifted for version v0.6.1
- [`5a9fe2d`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5a9fe2d820f79cf1fe8dfd58e4dfceeb39e80e87) fix: this is a new test7
- [`a459acd`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/a459acdad7f05c74868f0d0a945c9a1614d19ef2) break: this is a new test6

## [v0.6.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.6.0) - 2025-02-12

- [`894ca12`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/894ca12bc68877aa9fa386bd2a6c8ab59f46084b) feat: this is a new test5
- [`b14b9e7`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b14b9e7bfb4aea32b8b5803da6620fc4b97169cb) feat: this is a new test4
- [`e60ca41`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e60ca418d543a4c8546f684f83cef7e315bcfa3a) break: this is a new test3
- [`816d7e2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/816d7e2a47231debba516d34001611554f61104a) fix: this is a new test2
- [`5c37c6c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5c37c6c8a6bed33579662942797cc37d037264f7) fix: this is a new test1

## [v0.6.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.6.0) - 2025-02-12

- [`894ca12`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/894ca12bc68877aa9fa386bd2a6c8ab59f46084b) feat: this is a new test5
- [`b14b9e7`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b14b9e7bfb4aea32b8b5803da6620fc4b97169cb) feat: this is a new test4
- [`e60ca41`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e60ca418d543a4c8546f684f83cef7e315bcfa3a) break: this is a new test3
- [`816d7e2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/816d7e2a47231debba516d34001611554f61104a) fix: this is a new test2
- [`5c37c6c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5c37c6c8a6bed33579662942797cc37d037264f7) fix: this is a new test1

## [v0.5.1](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.5.1) - 2025-02-12

- [`ebb1c71`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/ebb1c71920e114a32d43893b23bcdee319fdfb5b) fix: this is a new test
- [`a871a20`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/a871a20b9672adf3ae1a63516f7912245e105120) ci(uplift): uplifted for version v0.5.0

## [v0.5.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.5.0) - 2025-02-12

- [`782f99e`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/782f99eb471eadc0ca4ce271dae5826261428d63) feat: bumping
- [`ee30401`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/ee3040143bb5fdc272a36890cb947a745db323a4) feat: bumping
- [`3fa7f48`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/3fa7f486175dfe096720d676bf7bead048832e4f) fix: manually update VERSION file

## [v0.5.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.5.0) - 2025-02-12

- [`782f99e`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/782f99eb471eadc0ca4ce271dae5826261428d63) feat: bumping
- [`4ebc5e3`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/4ebc5e323812e73939013a3f9a056d803992707e) ci(uplift): uplifted for version v0.5.0
- [`ee30401`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/ee3040143bb5fdc272a36890cb947a745db323a4) feat: bumping
- [`3fa7f48`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/3fa7f486175dfe096720d676bf7bead048832e4f) fix: manually update VERSION file

## [v0.4.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.4.0) - 2025-02-12

- [`8eac0b7`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/8eac0b740468403561ff0e239db1761e53379c7e) feat: bumping test
- [`2c67392`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/2c67392b65a74164384eb0999f2b3203a39aef44) feat: bumping test
- [`8d4d9ba`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/8d4d9ba941337a720d7c78fead6257fbf731c2df) break: bumping test

## [v0.3.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.3.0) - 2025-02-12

- [`c1dcfb4`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/c1dcfb47ff5fa605e19c460464a002e2deaadbb7) break: mod changelog
- [`bbfffc6`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/bbfffc67d5efbb9833db1961cba1d4ed1c0997a5) ci(uplift): uplifted for version v0.2.0
- [`b641550`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b641550df3f47073eb87469156cb66cb33ac797f) ci(uplift): uplifted for version
- [`6c0a010`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/6c0a010ef0603652af7d116b15d030f68cb10834) feat: this is a test
- [`d61ccd7`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d61ccd7bcbfa4d0a176487b46d0cac3607eeaed7) feat: this is a test
- [`beed04a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/beed04a094328233366cefd2a769abf499c209ad) feat: this is a test
- [`8b5dfe8`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/8b5dfe8fa91fd1aea105f1bd9d9ae2ec61c5ca96) fix: this is a test
- [`9c76e6f`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/9c76e6f9575186cedbd078023b73b83e9cc26704) fix: this is a test

## [v0.2.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.2.0) - 2025-02-12

- [`e2a4df6`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e2a4df6bb4f68b01ffbc10a9a1c21aefb4e9ff18) feat: test

## [v0.2.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.2.0) - 2025-02-12

- [`e2a4df6`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e2a4df6bb4f68b01ffbc10a9a1c21aefb4e9ff18) feat: test

## [v0.2.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.2.0) - 2025-02-12

- [`e2a4df6`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e2a4df6bb4f68b01ffbc10a9a1c21aefb4e9ff18) feat: test

## [v0.1.0](https://gitlab.com/Idan-Maimon/demo-app-be/-/tags/v0.1.0) - 2025-02-12

- [`8882567`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/8882567d1649c52406dfadcfe7d4c25c6602d5dc) fix: test
- [`7698c5f`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/7698c5fe9aa95e0216ce0decc90e4cd1fe54df14) fix: changelog modified
- [`86437d9`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/86437d9e74daaf87d70e24fa6f503eb2c7e3b0bc) feat: checking uplift
- [`911e577`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/911e577cedf8a98c68cbd1372033afd761c9874a) feat/testing uplift
- [`355972a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/355972ada248f7e0839ea3752209583b0feae7da) Add changelog for version 6.0.1
- [`e6bcba9`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e6bcba90ab482514a85aa3d78ab7e0f47a600d9c) fix: this is a test

  checking feature
  removing feature
  adding feature

  Changelog: added
- [`f75623c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/f75623c5b412b388d6b5619759e5118d1c2f5a43) Add changelog for version 6.0.0
- [`c69f6f4`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/c69f6f441b318628575a526b38e9d6e5363a8714) Bump version: 1.5.0 → 1.5.1
- [`2ba264a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/2ba264a2e9f19538bd867e7a596ab2c3effc3ff2) fix/ this is test
  checking feature
  removing feature
  adding feature
  Changelog: added
- [`8cfa42c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/8cfa42cd6e5472a9b9a58f8ce4630f0e14df12cf) Bump version: 1.4.0 → 1.5.0
- [`601422c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/601422c2e7583e1f856b2df1ac76a2e8afeddb16) feat/checking feature
  Changelog: added
- [`b4f1d4d`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b4f1d4d1737027e0ba1c9e178f32a69998be842e) feat/checking feature
  Changelog: added
- [`eed0ee3`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/eed0ee39a9e2a41d7ebd6da8fde8e3fbe1b3cf9b) feat/checking feature
  Changelog: added
- [`027eba1`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/027eba146f510289cd59bbdb0e3ee0a505a1e51a) Bump version: 1.3.0 → 1.4.0
- [`823dbf9`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/823dbf976320fa62cbd9ed1388c0105b29659e41) feat/checking feature
  Changelog: added
- [`674a773`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/674a77396885495025f8f1d64ffdbad83f3c36e6) fix/modifying ci release stage
  Changelog: changed
- [`a87f9a6`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/a87f9a684ce945d97d27882115c508f9051bb9d8) fix/modifying ci release stage
  Changelog: changed
- [`a3fb0d8`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/a3fb0d88e5e599db306b0775ec895de7601a7533) fix/fixing release
  Changelog: changed
- [`1a71a5f`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/1a71a5f4d6a32f7594d21150e0c19049d9e6d49b) Bump version: 1.2.0 → 1.3.0
- [`beadd8c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/beadd8c0cdf6ee0093f50bd36829eb36c0174dc5) feat/adding release management feature

  Changelog: added
- [`c14a643`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/c14a6430e592de45ce7287ff01a4d38467240583) Bump version: 1.1.0 → 1.2.0
- [`635ac39`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/635ac390bbfa1ba04bf2fa8a1f080d29b0c1f651) feat/test-version-bump
- [`94d025a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/94d025a96eaf254c5782379ba4b0e0136bb4c4ac) mod ci
- [`424033b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/424033b71aa1bc5ccb86abfaa82ae28900b62112) Bump version: 1.0.0 → 1.1.0
- [`9186577`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/9186577387c05350b125876b758dece1fc31db72) feat/new
- [`054f467`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/054f46707e68f0d912fd63728c78541916fcf94c) feat/new
- [`317b28c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/317b28cfeda5c59506b5b6f7c6ff15afa69f19b7) Merge branch 'feat/versioning' into 'main'

  trying automated versioning 1

  See merge request Idan-Maimon/demo-app-be!1
- [`6f70abe`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/6f70abe543a43e124667969964d9af8d4fe5a2ce) trying automated versioning 1
- [`62d09ae`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/62d09aef8babd55c1d1256c9e7f96cea6f9180a9) testing CI
- [`39ee8e2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/39ee8e2d80faa3e8878debac7f6993cfc3f08d6e) testing ci
- [`be41c69`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/be41c69ecace255051bd88496321f3865b24767e) testing ci
- [`2dd4522`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/2dd452213513ca2c648199014b438ab3ae154c9c) testing ci
- [`07ae0c1`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/07ae0c11807e49e8326139d70ebb8f258c6a1a05) test ci
- [`465c7a8`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/465c7a8f463ec4d51de7496dee901286723c5199) fixed deploy rule in CI
- [`488624c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/488624c7f32ebe6751208aad96f00a08d7dc173c) edited CI
- [`75384ba`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/75384babf9d19905f57475d9a7ee4145c1e3275e) Added condition to unit testing
- [`13d3abf`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/13d3abf16316f300dd67592bdd4fd012166e050c) ci test
- [`089d3d3`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/089d3d3b0f53290e7e72c9febde274a8de8b615b) test
- [`6405b58`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/6405b58106c35823e1a8bd134ead8ecad095a805) test new ci
- [`77dcb0a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/77dcb0a60926fa237cad76383b0babec420cbe05) fixed unit tests
- [`54777fd`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/54777fd9c4a0c2fe99fff51da5c0a4e27b452793) added POST drink to test unit
- [`b623c76`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b623c76c05cbc29cfb2472118710010350ea72c5) testing unit
- [`07b6726`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/07b6726fad615c358e87e4a4ad9007e791ed93f0) testing unit tests
- [`23e4ce1`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/23e4ce1ddc7c136f485a921d330e01d7dc95d60e) modifying CI pipeline
- [`4fe643b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/4fe643b36176c5d3c92d6fe3c00de86ad33de060) Update .gitlab-ci.yml file
- [`5d484be`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5d484bef6a86c121d61be7ed61b1c6e4456bdab1) Update .gitlab-ci.yml file
- [`df4b93b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/df4b93b08e8db08005cbff063741f93d9a6b58aa) Update .gitlab-ci.yml file
- [`06d526a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/06d526a4da4a32d318d726543b5585660776ae47) Update file .gitlab-ci.yml
- [`d84f5ba`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d84f5ba0c59e60a8dcf603c3fe01048365e31ed6) Update .gitlab-ci.yml file
- [`3562e97`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/3562e976c015127e9e1938dbf4056cb725121ced) Update .gitlab-ci.yml file
- [`e2eef38`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e2eef3869adc34e5c91f13b87b14bacc1d8aa91e) Update .gitlab-ci.yml file
- [`30ec7dc`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/30ec7dc881ba95477649df39a1be8bffbc129976) Update .gitlab-ci.yml file
- [`0d4a44a`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/0d4a44a0289fc0ed1e2911ba2ee12f9d946ae1a6) Update .gitlab-ci.yml file
- [`151db93`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/151db9388354dc5ab98ee06b49cb58b7198d2385) Update .gitlab-ci.yml file
- [`e1a65ef`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e1a65ef7ae8c0f61f97a14ef3f185cf312bcc808) Update .gitlab-ci.yml file
- [`aa95c9b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/aa95c9b7b6fb229aa867688ba806ea53c9b86a4b) Update 2 files

  - /.gitlab-ci-old.yml
  - /.gitlab-ci.yml
- [`1371a58`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/1371a5854f36ffc8d4689ee746f6451903ffb158) Update file .gitlab-ci.yml
- [`36c8a94`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/36c8a9490fb7299e4500d9c30d199be0be73325b) Update file .gitlab-ci.yml
- [`b804b7c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b804b7c88690750c9f569ff77aa83b564972300d) Update file .gitlab-ci.yml
- [`b2ff15f`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b2ff15f2fce607f27d2bddbd171a6b1a02ab77f4) Update file .gitlab-ci.yml
- [`ccd33a6`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/ccd33a6d817f07e2d58573091301a45fa50e8a33) Update file .gitlab-ci.yml
- [`3a30c82`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/3a30c82693ec78663a510c73d8f26b9c515ccef8) Update 2 files

  - /test_app_unit.py
  - /.gitlab-ci.yml
- [`11e003d`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/11e003db130fa7b43873635cc966eb2df2e280f9) Update file test_app_unit.py
- [`b77dcf1`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/b77dcf163c3284da04a95d2e480aa8fef35dfed6) mod
- [`ced5f29`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/ced5f29be700ee92842c5e4ecd10aa67e878a25f) mod
- [`24018f0`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/24018f08a91c9bf93f94aa755435d0c0011c8861) mod
- [`72c48c0`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/72c48c02c79d90aee7492a7eb338ca1c80b1b768) mod
- [`0f48e22`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/0f48e221ce0fb143770eb0fd2b4382d6cb453fa1) added image
- [`c565e92`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/c565e92c9a0d6f92c02806584ad8cefcacc2e5f2) added unit
- [`bff070f`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/bff070febf786edd9800e4f54d582289c0b2af41) changed sleep from 300 to 130 sec
- [`cfa95c7`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/cfa95c7c3ce86273e815feabf243c6aea0bcb194) adding final step: deploy to prod
- [`48280be`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/48280be81a0299050e2c81a43f0d46cf67b8bf3c) adding final step: deploy to prod
- [`677fb91`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/677fb91acbb5664503d3eb3a40522f4ec73d612b) adding final step: deploy to prod
- [`4f457ba`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/4f457baafefaddbaf21421d4938410a4b3a342e4) fixing ci
- [`1806891`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/18068917abfbf5c84fef6eef9f335d1730926cbe) fixing ci
- [`9f9ac04`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/9f9ac04e259c3befa5a66763b2a6222996893626) fixing ci
- [`98d02dd`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/98d02dd4b4a808cdb7eafabe0d622a955a9764b0) fixing ci
- [`23d2ac2`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/23d2ac2f09e5dcce19dd5cfc145f917dd0ca7a58) fixing ci
- [`f28de94`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/f28de94620015135204aed93fbcdd9b94d6e39d7) fixing ci
- [`e39d52e`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/e39d52eea599db705dbce3e0b5f6b7fc838d1eca) fixing ci
- [`ddebb4b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/ddebb4b8be2b394e884ee515a7d1a448733d6bb3) fixing ci
- [`410d49d`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/410d49da7a2f40f76aafb37f19cc2894647d902b) fixing ci
- [`f2d6c50`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/f2d6c509ce7a4565c8af77af90f41da2a8161a64) fixing ci
- [`bbe9073`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/bbe9073d8570dc68fa7b2b18f333aa4b2dd663fb) fixing ci
- [`c9a45c8`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/c9a45c8d2e6c5f08f3a3452424bdeb71eb6d87c9) fixing ci
- [`1708958`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/17089585a32949bc768fd5d01365a44c2600682b) fixing ci
- [`336739c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/336739c358f1b734cdc05cf6ff5de99af8c62972) fixing ci
- [`052bd23`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/052bd23ce08e4525ac35fc4913f39e389cf7470d) fixing sed command
- [`8ac63c6`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/8ac63c678b7f36bfc4f39d4b92a0bc5f6514992e) fixing sed command
- [`699964c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/699964c591354db4841a3c4e5030f6db61a6bda3) changed delimiter from | to # at gitlab-ci
- [`49f9471`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/49f947135c37d8c0f75199e5104c8acb509c84c0) update gitlab-ci to push changes to dev deployment
- [`be4e66c`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/be4e66c60d6aaee4345e3284771991d02556587a) added api-test to CI pipeline
- [`5cad2b3`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5cad2b37dbf3da19c5ebeed76ac9e6b98bd764cb) fixed image tag
- [`567bebf`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/567bebfefa9b2c9c85831c810a23cdf06493903c) fixed image tag
- [`72c4658`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/72c465893c913d691cf7fbb9c36bdd5b055be3be) fixed code to allow access to openapi-spec.json
- [`51284cf`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/51284cf119f298117e16042d7f7a097b4a1d386b) fixed syntax error inside dockerfile
- [`662069b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/662069be0734d93b2d9e07a6d695d122437760ca) adding openapi-spec.json
- [`2024d38`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/2024d381b92842cfb8255125716fd93693bae398) added openapi route
- [`463608f`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/463608ff6b9834bf196d5827ea7f940da0d30b7c) removed db migration
- [`16fbcca`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/16fbcca275cece55b6a75e4a23b95cf1624cbbca) added api-scheme and fix code issues
- [`d38dc91`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d38dc91a4c35ec75bcfd6dff4fbaaec5db40d231) gitlab-ci push
- [`f91f5ab`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/f91f5abd03e87e52b748b5bbe611c9d0eb07382e) docker login
- [`d501dc5`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d501dc5ee5a45b66b779123e54b68e13ddfff296) added upgrade pip as part of the build process
- [`cb81b4b`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/cb81b4b560ff895256ca171ebdc9b3ca5bb90221) Update .gitlab-ci.yml file
- [`4c672e6`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/4c672e6f101373dab0acc02a323569613bd23582) added requirements.txt
- [`8394c84`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/8394c844846e2a02db297cc8b3a3f9368ac28ac5) Update .gitlab-ci.yml file
- [`7da7bb7`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/7da7bb7d8c1a7aa2156da7271a0bb4c2b5d1be34) Update .gitlab-ci.yml file
- [`7f69368`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/7f69368858027c1bf6408d5e6e90f118845c3008) Update .gitlab-ci.yml file
- [`2b720fa`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/2b720fac4515ec968c55cebc69592b366f59f519) Update .gitlab-ci.yml file
- [`eac62da`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/eac62da1c6281ef8673a8384cf28b90685995f3f) Update .gitlab-ci.yml file
- [`778b5a8`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/778b5a8e45ad8d8f9f26d4a3f3c55471747a6e95) Update .gitlab-ci.yml file
- [`5ba15e9`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/5ba15e9b099e7f3a33b34e4ade27effb6f8086ea) first commit. adding Dockerfile and flask code
- [`d62fdc9`](https://gitlab.com/Idan-Maimon/demo-app-be/-/commit/d62fdc9b0d36e80ed2e797e0907f6c9393e88fc8) Initial commit

## 2.0.0 (2025-03-04)

No changes.
