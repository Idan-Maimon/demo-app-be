import os
import json
from flask import Flask, request, Response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError
from flask_cors import CORS

#Create the app
app = Flask (__name__)
CORS(app)

#Config the DB
db_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'db-storage/data.db')
if not os.path.exists(os.path.dirname(db_path)):
    os.makedirs(os.path.dirname(db_path))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(db_path)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#Create the extension
db = SQLAlchemy(app)

# Define the Drink model
class Drink(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    description = db.Column(db.String(120))

    def __repr__(self):
        return '<Drink %r>' % self.name

# Create the database and tables if they don't exist
with app.app_context():
    db.create_all()
    os.chmod(db_path, 0o777)

@app.route('/api/openapi.json', methods=['GET'])
def get_openapi_schema():
    # Load the OpenAPI schema from the file
    schema_file_path = '/app/openapi-spec.json'  # Adjust the path if needed
    with open(schema_file_path, 'r') as schema_file:
        schema_data = json.load(schema_file)

    # Create a JSON response containing the schema
    response = Response(
        json.dumps(schema_data, indent=2),
        content_type='application/json',
    )

    return response

@app.route('/')
def index():
  return ('This is my bar menue, please press here')

@app.route('/api/drinks')
def get_drinks():
    drinks = Drink.query.all()
    output = []
    for drink in drinks:
        drink_data = {'name': drink.name, 'description': drink.description, 'id': drink.id}
        output.append(drink_data)
    return {"drinks": output}

@app.route('/api/drinks/<id>') 
def get_drink(id):
    drink = Drink.query.get_or_404(id)
    return {"name": drink.name, "description": drink.description}

@app.route('/api/drinks', methods=['POST'])
def add_drink():
    name = request.json.get('name')
    description = request.json.get('description')

    if not name or not description:
        return {'error': 'Name or description cannot be null'}, 400  # Return HTTP status code 400 for bad request
    try:
        drink = Drink(name=name, description=description)
        db.session.add(drink)
        db.session.commit()
        return {'id': drink.id}, 201  # Return HTTP status code 201 for successful creation
    except IntegrityError:
        db.session.rollback()  # Rollback the transaction to avoid partial data insertion
        return {'error': 'Drink name already exists'}, 409  # Return HTTP status code 409 for conflict

@app.route('/api/drinks/<id>', methods=['DELETE'])
def delete_drink(id):
    drink = Drink.query.get(id)
    if drink is None:
        return {"error": "Drink Not Found"}, 404 # Return HTTP status code 404 for drink not found
    db.session.delete(drink)
    db.session.commit()
    return {'Drink was deleted': drink.name}

if __name__ == '__main__':
   app.run(host='0.0.0.0',port=5000, debug=True)